#!/bin/bash
echo on    

echo '###########################################################'
echo '#Script para buildar os repositorios do projeto da rumo.  #'
echo '#                                                         #'
echo '#Building local                                           #'
echo '###########################################################'
echo '#Autor: Deovan Zanol                                      #'
echo '###########################################################'
echo '###########################################################'
echo Building Controle de perdas.
cd ../controleperdas
mvn clean package -DskipTests  -Plocal
cp ./target/app*.jar ../core-dockers-cdp/controleperdas/app.jar
echo '###########################################################'
echo Building Controle de perdas configuracoes.
cd ../controleperdasconfiguracoes
mvn clean package -DskipTests  -Plocal
cp ./target/app*.jar ../core-dockers-cdp/controleperdasconfiguracoes/app.jar
echo '###########################################################'
echo Building cadastros.
cd ../cadastros
mvn clean package -DskipTests  -Plocal-oracle
cp ./target/app*.jar ../core-dockers-cdp/cadastros/app.jar
echo '###########################################################'
echo Building malhas.
cd ../malhas
mvn clean package -DskipTests  -Plocal-oracle
cp ./target/app*.jar ../core-dockers-cdp/malhas/app.jar
echo '###########################################################'
echo Building notificacoes.
cd ../notificacoes
mvn clean package -DskipTests  -Plocal
cp ./target/app*.jar ../core-dockers-cdp/notificacoes/app.jar
echo '###########################################################'
echo Building sindicancias.
cd ../sindicancias
mvn clean package -DskipTests  -Plocal-oracle
cp ./target/app*.jar ../core-dockers-cdp/sindicancias/app.jar
echo '###########################################################'
echo Building comumferroviasauxiliares.
cd ../comumferroviasauxiliares
mvn clean package -DskipTests  -Plocal-oracle
cp ./target/app*.jar ../core-dockers-cdp/comumferroviasauxiliares/app.jar
echo '###########################################################'
echo '#Building docker controleperdas                           #'
echo '###########################################################'
cd    ../core-dockers-cdp/controleperdas
docker build -t controleperdas .
echo '###########################################################'
echo '#Building docker controleperdasconfiguracoes              #'
echo '###########################################################'
cd    ../controleperdasconfiguracoes
docker build -t controleperdasconfiguracoes .
echo '###########################################################'
echo '#Building docker cadastros                                #'
echo '###########################################################'
cd    ../cadastros
docker build -t cadastros .
echo '###########################################################'
echo '#Building docker malhas                                   #'
echo '###########################################################'
cd    ../malhas
docker build -t malhas .
echo '###########################################################'
echo '#Building docker notificacoes                             #'
echo '###########################################################'
cd    ../notificacoes
docker build -t notificacoes .
echo '###########################################################'
echo '#Building docker sindicancias                             #'
echo '###########################################################'
cd    ../sindicancias
docker build -t sindicancias .
echo '###########################################################'
echo '#Building docker comumferroviasauxiliares                 #'
echo '###########################################################'
cd    ../comumferroviasauxiliares
docker build -t comumferroviasauxiliares .
echo '###########################################################'
echo '#Repositorios Building com sucesso!                       #'
echo '###########################################################'
sleep 10s