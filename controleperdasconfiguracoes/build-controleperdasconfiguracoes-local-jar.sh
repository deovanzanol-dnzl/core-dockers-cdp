#!/bin/bash
echo on    

echo '###########################################################'
echo '#Script para buildar os repositorios do projeto da rumo.  #'
echo '#                                                         #'
echo '#Builda local                                             #'
echo '###########################################################'
echo '#Autor: Deovan Zanol                                      #'
echo '###########################################################'
echo '###########################################################'
echo Buildando controleperdasconfiguracoes.
cd ../../controleperdasconfiguracoes
mvn clean package -DskipTests  -Plocal
cp ./target/app*.jar ../core-dockers-cdp/controleperdasconfiguracoes/app.jar
cd ../core-dockers-cdp
echo '###########################################################'
echo '#Building docker controleperdasconfiguracoes              #'
echo '###########################################################'
cd './controleperdasconfiguracoes'
docker build -t controleperdasconfiguracoes .
echo '###########################################################'
echo '#Repositorios buildado com sucesso!                       #'
echo '###########################################################'

sleep 10s