#!/bin/bash
echo on    

echo '###########################################################'
echo '#Script para buildar os repositorios do projeto da rumo.  #'
echo '#                                                         #'
echo '#Builda local                                             #'
echo '###########################################################'
echo '#Autor: Deovan Zanol                                      #'
echo '###########################################################'
echo '###########################################################'
echo Buildando cadastros.
cd ../../cadastros
mvn clean package -DskipTests  -Plocal
cp ./target/app*.jar ../core-dockers-cdp/cadastros/app.jar
cd ../core-dockers-cdp
echo '###########################################################'
echo '#Building docker cadastros                             #'
echo '###########################################################'
cd './cadastros'
docker build -t cadastros .
echo '###########################################################'
echo '#Repositorios buildado com sucesso!                       #'
echo '###########################################################'

sleep 10s