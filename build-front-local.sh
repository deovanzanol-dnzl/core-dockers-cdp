#!/bin/bash
echo on    


echo ###########################################################
echo #Script para buildar os repositorios do projeto da rumo.  #
echo #                                                         #
echo #Builda local                                             #
echo ###########################################################
echo #Autor: Deovan Zanol                                      #
echo ###########################################################
echo ###########################################################
echo Buildando controle-perdas-frontend.
cd ../controle-perdas-frontend
npm install
npm run build -- --configuration=qa-gft --base-href=//web --deploy-url=//
rm -r '../core-dockers/controle-perdas-frontend/dist/'
cp -r ./dist  '../core-dockers/controle-perdas-frontend/dist/'
cp nginx-custom.conf  '../core-dockers/controle-perdas-frontend/'
cd ../core-dockers-cdp
echo ###########################################################
echo #Building docker!                                         #
echo ###########################################################
cd './controle-perdas-frontend'
docker build -t controle-perdas-frontend .
echo ###########################################################
echo #Build concluido!                                         #
echo ###########################################################

sleep 10s