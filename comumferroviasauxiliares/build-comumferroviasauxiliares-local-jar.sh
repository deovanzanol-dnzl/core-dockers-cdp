#!/bin/bash
echo on    

echo '###########################################################'
echo '#Script para buildar os repositorios do projeto da rumo.  #'
echo '#                                                         #'
echo '#Builda local                                             #'
echo '###########################################################'
echo '#Autor: Deovan Zanol                                      #'
echo '###########################################################'
echo '###########################################################'
echo Buildando comumferroviasauxiliares.
cd ../../comumferroviasauxiliares
mvn clean package -DskipTests  -Plocal
cp ./target/app*.jar ../core-dockers-cdp/comumferroviasauxiliares/app.jar
cd ../core-dockers-cdp
echo '###########################################################'
echo '#Building docker comumferroviasauxiliares                 #'
echo '###########################################################'
cd './comumferroviasauxiliares'
docker build -t comumferroviasauxiliares .
echo '###########################################################'
echo '#Repositorios buildado com sucesso!                       #'
echo '###########################################################'

sleep 10s