#!/bin/bash
echo on    
echo ###########################################################
echo #Script para clonar os repositorios do projeto da rumo.   #
echo #                                                         #
echo ###########################################################
echo #Autor: Deovan Zanol                                      #
echo ###########################################################
echo ###########################################################
echo Clonando controle-perdas-frontend.
cd ..
git clone git@bitbucket.org:rumo_logistica/controle-perdas-frontend.git
cd ./controle-perdas-frontend
git checkout QA-GFT
echo ###########################################################
echo Clonando teste-automatizado-controle-perdas.
cd ..
git clone git@bitbucket.org:rumo_logistica/teste-automatizado-controle-perdas.git
cd ./teste-automatizado-controle-perdas
git checkout QA-GFT
echo ###########################################################
echo Clonando controleperdas.
cd ..
git clone git@bitbucket.org:rumo_logistica/controleperdas.git
cd ./controleperdas
git checkout QA-GFT
echo ###########################################################
echo Clonando controleperdasconfiguracoes.
cd ..
git clone git@bitbucket.org:rumo_logistica/controleperdasconfiguracoes.git
cd ./controleperdasconfiguracoes
git checkout QA-GFT
echo ###########################################################
echo Clonando cadastros.
cd ..
git clone git@bitbucket.org:rumo_logistica/cadastros.git
cd ./cadastros
git checkout QA-GFT
echo ###########################################################
echo Clonando malhas.
cd ..
git clone git@bitbucket.org:rumo_logistica/malhas.git
cd ./malhas
git checkout QA-GFT
echo ###########################################################
echo Clonando notificacoes.
cd ..
git clone git@bitbucket.org:rumo_logistica/notificacoes.git
cd ./notificacoes
git checkout QA-GFT
echo ###########################################################
echo Clonando sindicancias.
cd ..
git clone git@bitbucket.org:rumo_logistica/sindicancias.git
cd ./sindicancias
git checkout QA-GFT
echo ###########################################################
echo Clonando comumferroviasauxiliares.
cd ..
git clone git@bitbucket.org:rumo_logistica/comumferroviasauxiliares.git
cd ./comumferroviasauxiliares
git checkout develop
echo ###########################################################
echo #Repositorios clonado com sucesso!                        #
echo ###########################################################
sleep 10s