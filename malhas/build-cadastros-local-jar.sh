#!/bin/bash
echo on    

echo '###########################################################'
echo '#Script para buildar os repositorios do projeto da rumo.  #'
echo '#                                                         #'
echo '#Builda local                                             #'
echo '###########################################################'
echo '#Autor: Deovan Zanol                                      #'
echo '###########################################################'
echo '###########################################################'
echo Buildando malhas.
cd ../../malhas
mvn clean package -DskipTests  -Plocal
cp ./target/app*.jar ../core-dockers-cdp/malhas/app.jar
cd ../core-dockers-cdp
echo '###########################################################'
echo '#Building docker malhas                                   #'
echo '###########################################################'
cd './malhas'
docker build -t malhas .
echo '###########################################################'
echo '#Repositorios buildado com sucesso!                       #'
echo '###########################################################'

sleep 10s