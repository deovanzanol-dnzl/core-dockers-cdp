@echo on    
@echo '###########################################################''
@echo '#Script para atualizar os repositorios do projeto da rumo.#'
@echo '###########################################################'
@echo #Autor: Deovan Zanol                                      #
@echo '###########################################################'
@echo '###########################################################'
@echo '###########################################################'
@echo Atualizando controle-perdas-frontend.
cd ..\controle-perdas-frontend
git pull
@echo '###########################################################'
@echo Atualizando controleperdas.
cd ..\controleperdas
git pull
@echo '###########################################################'
@echo Atualizando controleperdasconfiguracoes.
cd ..\controleperdasconfiguracoes
git pull
@echo '###########################################################'
@echo Atualizando cadastros.
cd ..\cadastros
git pull
@echo '###########################################################'
@echo Atualizando malhas.
cd ..\malhas
git pull
@echo '###########################################################'
@echo Atualizando notificacoes.
cd ..\notificacoes
git pull
@echo '###########################################################'
@echo Atualizando sindicancias.
cd ..\sindicancias
git pull
@echo '###########################################################'
@echo Atualizando comumferroviasauxiliares.
cd ..\comumferroviasauxiliares
git pull
@echo '###########################################################'
@echo '#Atualizado!                                              #'
@echo '###########################################################'
timeout 10